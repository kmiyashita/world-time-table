import 'date-fns';

import DatePicker from '@mui/lab/DatePicker';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import TextField from '@mui/material/TextField';
import copyToClipboard from 'copy-to-clipboard';
import { DateTime } from 'luxon';
import React, { ReactElement } from 'react';
import { convertOffsetInMinutesToString,findTimeZone, TimeZoneSelectDialog } from 'react-timezone-map-select';

import { DEFAULT_TIME_ZONE_NAME } from './Constant';
import TimeTable from './TimeTable';
import { createLeftAlignedString } from './Util';

/**
 * Get the current date and time in UTC, rounded to hours.
 * @returns rounded date and time
 */
const getCurrentDateTimeRounded = (): Date => {
  const dateTime = new Date();
  dateTime.setMinutes(0);
  dateTime.setSeconds(0);
  dateTime.setMilliseconds(0);

  return dateTime;
}

/**
 * Compose user-friendly string showing a certain time in multiple time zones.
 * @param dateTime       date and time in UTC
 * @param timeZoneNames  array of time zone names
 * @return composed string
 */
const composeTimeRowString = (dateTime: DateTime, timeZoneNames: string[]): string => {
  const maxCityNameLength = Math.max(...timeZoneNames.map(tzName => {
    const timeZone = findTimeZone(tzName) || { countryName: 'unknown', mainCities: ['unknown'] };
    return (timeZone.countryName + ' / ' + timeZone.mainCities[0]).length;
  }));

  let composedString = '';
  timeZoneNames.forEach((tzName, index) => {
    const timeZone = findTimeZone(tzName) || { countryName: 'unknown', mainCities: ['unknown'] };
    const localTime = dateTime.setZone(tzName);
    const localTimeString = localTime.toLocaleString({
      weekday: 'short',
      month: 'short',
      day: '2-digit',
      hour: '2-digit',
      minute: '2-digit',
    });
    if (index !== 0) {
      composedString += '\n';
    }
    composedString
      += createLeftAlignedString(timeZone.countryName + ' / ' + timeZone.mainCities[0],
        maxCityNameLength)
      + ':  ' + localTimeString + ' ('
      + convertOffsetInMinutesToString(localTime.offset) + ')';
  });

  return composedString;
}

const TimeTableScreen = (): ReactElement => {
  /** Time zone names to show in TimeTable */
  const [timeZoneNames, setTimeZoneNames] = React.useState([
    DEFAULT_TIME_ZONE_NAME,
  ]);

  /** Representative date */
  const [representativeDate, setRepresentativeDate] = React.useState<Date>(
    getCurrentDateTimeRounded()
  );

  /** True if you are editing a certain time zone in the time table */
  const [openDialog, setOpenDialog] = React.useState(false);

  /** Index in timeZoneNames to edit. Only valid if openDialog === true */
  const [timeZoneIndexToEdit, setTimeZoneIndexToEdit] = React.useState(0);

  /** Set the representative date, and redraw the whole time table. */
  const handleDateChange = (date: unknown) => {
    if (date !== null && date instanceof DateTime) {
      setRepresentativeDate(date.toJSDate());
    }
  };

  /** Open the dialog to edit time zone */
  const handleOpenDialog = (timeZoneIndex: number) => {
    setTimeZoneIndexToEdit(timeZoneIndex);
    setOpenDialog(true);
  };

  /** Close the dialog to edit time zone */
  const handleCloseDialog = (newTimeZoneName: string) => {
    const newTimeZoneNames = [...timeZoneNames];
    newTimeZoneNames[timeZoneIndexToEdit] = newTimeZoneName;
    setTimeZoneNames(newTimeZoneNames);
    setOpenDialog(false);
  };

  /** Delete a time zone */
  const handleDeleteTimeZone = (timeZoneIndex: number) => {
    const newTimeZoneNames = [
      ...timeZoneNames.slice(0, timeZoneIndex),
      ...timeZoneNames.slice(timeZoneIndex + 1, timeZoneNames.length),
    ];
    setTimeZoneNames(newTimeZoneNames);
  };

  const handleAddTimeZone = () => {
    const newTimeZoneNames = [...timeZoneNames];
    newTimeZoneNames.push(DEFAULT_TIME_ZONE_NAME);
    setTimeZoneNames(newTimeZoneNames);
  };

  const handleCopyTimeZones = (dateTime: DateTime) => {
    const composedString = composeTimeRowString(dateTime, timeZoneNames);
    copyToClipboard(composedString, {
      format: 'text/plain'
    })
  };

  return (
    <div>
      <TimeZoneSelectDialog
        open={openDialog}
        timeZoneName={timeZoneNames[timeZoneIndexToEdit]}
        onClose={handleCloseDialog}
      />
      <Container>
        <Box>
          <DatePicker
            label=""
            inputFormat="MM/dd/yyyy"
            value={representativeDate}
            onChange={handleDateChange}
            renderInput={(params) => <TextField {...params} />}
          />
        </Box>
        <Box>
          <TimeTable
            timeZoneNames={timeZoneNames}
            onEditTimeZone={handleOpenDialog}
            onDeleteTimeZone={handleDeleteTimeZone}
            onAddTimeZone={handleAddTimeZone}
            onCopyTimeZones={handleCopyTimeZones}

            columnWidth={300}
            height={300}
            rowCount={100}
            rowHeight={35}
            width={1000}
            representativeDate={DateTime.fromJSDate(representativeDate)}
          />
        </Box>
      </Container>
    </div>
  );
};

export default TimeTableScreen;
